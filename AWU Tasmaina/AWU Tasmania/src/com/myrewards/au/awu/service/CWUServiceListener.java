package com.myrewards.au.awu.service;

public interface CWUServiceListener {
	public void onServiceComplete(Object response, int eventType);
}
