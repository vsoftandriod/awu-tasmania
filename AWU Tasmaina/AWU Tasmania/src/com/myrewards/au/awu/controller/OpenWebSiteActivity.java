package com.myrewards.au.awu.controller;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

@SuppressLint("SetJavaScriptEnabled")
public class OpenWebSiteActivity extends Activity {
	View loading;
	WebView webView;
	//Button backButton;
@Override
protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	setContentView(R.layout.web_site_my_union_contacts);
	webView=(WebView)findViewById(R.id.webview);
	//backButton.setOnClickListener(this);
	webView.getSettings().setJavaScriptEnabled(true);
	webView.getSettings().setAllowFileAccess(true);
	webView.getSettings().setLoadsImagesAutomatically(true);
	
	loading=(View)findViewById(R.id.loading);
	loading.setVisibility(View.GONE);
	try {
		webView.loadUrl(getResources().getString(R.string.website));
	} catch (Exception e) {
		Log.v("Hari--> OpenWebsite onImageLoadComplete", e.getMessage());
		Toast.makeText(getApplicationContext(), "Poor internet connection. Try again", Toast.LENGTH_LONG).show();
		e.printStackTrace();
	}
  }
}
