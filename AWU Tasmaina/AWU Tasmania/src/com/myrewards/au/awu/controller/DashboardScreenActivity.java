package com.myrewards.au.awu.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.au.awu.utils.DatabaseHelper;
import com.myrewards.au.awu.utils.Utility;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.readystatesoftware.viewbadger.BadgeView;

/**
 * 
 * @author HARI This class is used to display like menu
 */

@SuppressLint("ShowToast")
public class DashboardScreenActivity extends Activity implements
		OnClickListener {

	// custom dialog fields
	public TextView logoutText, alertTitleTV;
	public Button loginbutton1;
	public Button cancelbutton1;
	final private static int LOGOUT = 1;
	public ImageView lalertImg;
	public TextView textLog;
	ImageView noticeboardBadge;
	DatabaseHelper helper;
	static int noticeCount = 0;
	public ImageView noticeboardunread;
	public static boolean noticeboardcount = false;
	boolean doubleBackToExitPressedOnce = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dashboard1);

		// Push Notifications Adding................... AWU
		try {
			Parse.initialize(this, "fv6OJuldgXS5HBPzHsd0Udw6ulWweI9sWPoV1qvq", "ODFzPrmSuDEoDLifkhTiKVdGQMSKvSA2ZW4qvGgx");

			PushService.setDefaultPushCallback(this, AppPushNotificationActivity.class);
			ParseInstallation.getCurrentInstallation().saveInBackground();
			ParseAnalytics.trackAppOpened(getIntent());
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		
		//PushService.subscribe(getApplicationContext(), "CWUChannel", DashboardScreenActivity.class);

		helper = new DatabaseHelper(DashboardScreenActivity.this);
		try {
			// this is for notifying the user regarding the updates of notice boards
			if (helper.getNoticeUpdateState().equals("NO_VALUE")) {
				helper.setNoticeUpdateState("ON");
				Utility.setNotificationReceiver(this);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerIVID);
		headerImage.getLayoutParams().height = Utility.screenHeight / 10;

		RelativeLayout myUnionRL = (RelativeLayout) findViewById(R.id.myUnionRLID);
		myUnionRL.setOnClickListener(this);
		RelativeLayout myDelegatesRL = (RelativeLayout) findViewById(R.id.myDelegatesRLID);
		myDelegatesRL.setOnClickListener(this);
		RelativeLayout myCardRL = (RelativeLayout) findViewById(R.id.myCardRLID);
		myCardRL.setOnClickListener(this);
		RelativeLayout myUnionContactsRL = (RelativeLayout) findViewById(R.id.myUnionContactsRLID);
		myUnionContactsRL.setOnClickListener(this);
		RelativeLayout myNoticeBoardRL = (RelativeLayout) findViewById(R.id.myNoticeBoardRLID);
		myNoticeBoardRL.setOnClickListener(this);
		RelativeLayout sendAFriendRL = (RelativeLayout) findViewById(R.id.sendAFriendRLID);
		sendAFriendRL.setOnClickListener(this);
		RelativeLayout mySpecialOffersRL = (RelativeLayout) findViewById(R.id.mySpecialOffersRLID);
		mySpecialOffersRL.setOnClickListener(this);
		RelativeLayout searchMyRewardsRL = (RelativeLayout) findViewById(R.id.searchMyRewardsRLID);
		searchMyRewardsRL.setOnClickListener(this);
		// set logout from cepu
		RelativeLayout lagoutRL = (RelativeLayout) findViewById(R.id.lagoutRLID);
		lagoutRL.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDialog(LOGOUT);
			}
		});

		RelativeLayout myParkingTimeRL = (RelativeLayout) findViewById(R.id.myParkingTimeRLID);
		myParkingTimeRL.setOnClickListener(this);
		RelativeLayout myissueRL = (RelativeLayout) findViewById(R.id.myissueRLID);
		myissueRL.setOnClickListener(this);
		RelativeLayout myAccountRL = (RelativeLayout) findViewById(R.id.myAccountRLID);
		myAccountRL.setOnClickListener(this);

		noticeboardunread = (ImageView) findViewById(R.id.noticeBadgeID);
		BadgeView badge = new BadgeView(DashboardScreenActivity.this,noticeboardunread);
		try {
			if (helper.getNoticeDetails() != 0) {
				badge.setText(Integer.toString(helper.getNoticeDetails()));
				badge.setBadgeBackgroundColor(Color.parseColor("#A8AE00"));
				badge.setBadgePosition(BadgeView.POSITION_BOTTOM_LEFT);
				badge.show();
			} else {
				badge.setVisibility(View.GONE);
				noticeboardunread.setVisibility(View.GONE);
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	// to create custom alerts
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog dialogDetails = null;
			switch (id) {
			case LOGOUT:
				LayoutInflater inflater = LayoutInflater.from(this);
				View dialogview = inflater.inflate(
						R.layout.dialog_layout_logout, null);
				AlertDialog.Builder dialogbuilder = new AlertDialog.Builder(
						this);
				dialogbuilder.setCancelable(false);
				dialogbuilder.setView(dialogview);
				dialogDetails = dialogbuilder.create();

				break;
			}
			return dialogDetails;
		}
		return null;
	}

	protected void onPrepareDialog(int id, Dialog dialog) {
		if (id == 1) {
			switch (id) {
			case LOGOUT:
				final AlertDialog alertDialog1 = (AlertDialog) dialog;
				alertTitleTV = (TextView) alertDialog1
						.findViewById(R.id.alertLogoutTitleTVID);
				alertTitleTV.setTypeface(Utility.font_bold);
				logoutText = (TextView) alertDialog1
						.findViewById(R.id.my_logoutTVID);
				logoutText.setTypeface(Utility.font_reg);
				loginbutton1 = (Button) alertDialog1
						.findViewById(R.id.loginBtnH_ID);
				loginbutton1.setTypeface(Utility.font_bold);
				cancelbutton1 = (Button) alertDialog1
						.findViewById(R.id.btn_cancelH_ID);
				cancelbutton1.setTypeface(Utility.font_bold);
				alertDialog1.setCancelable(false);
				loginbutton1.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						try {
							helper.deleteLoginDetails();
						} catch (Exception e) {
							if (e != null) {
								Log.w("Hari-->DEBUG", e);
								e.printStackTrace();
							}
						}
						Intent logoutIntent = new Intent(
								DashboardScreenActivity.this,
								LoginScreenActivity.class);
						logoutIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
								| Intent.FLAG_ACTIVITY_SINGLE_TOP);
						startActivity(logoutIntent);
						LoginScreenActivity.etUserId.setText("");
						LoginScreenActivity.etPasswd.setText("");
						DashboardScreenActivity.this.finish();
						alertDialog1.dismiss();
					}
				});
				cancelbutton1.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						alertDialog1.dismiss();
					}
				});
				break;
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.myUnionRLID:
			Intent intentMyUnion = new Intent(DashboardScreenActivity.this,
					AboutUsActivity.class);
			intentMyUnion.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_UNION);
			startActivity(intentMyUnion);
			break;
		case R.id.myDelegatesRLID:
			Intent intentMyDelegates = new Intent(DashboardScreenActivity.this,
					MyDealsActivity.class);
			intentMyDelegates.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_DELEGATES);
			startActivity(intentMyDelegates);
			break;
		case R.id.myCardRLID:
			try {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentMyCard = new Intent(DashboardScreenActivity.this,
							MyCardActivity.class);
					intentMyCard.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_CARD);
					startActivity(intentMyCard);
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.myUnionContactsRLID:
			Intent intentMyUnionContacts = new Intent(
					DashboardScreenActivity.this, MyUnionContactsActivity.class);
			intentMyUnionContacts.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.MY_UNION_CONTACTS);
			startActivity(intentMyUnionContacts);
			break;
		case R.id.myNoticeBoardRLID:
			try {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentMyNoticeBoard = new Intent(
							DashboardScreenActivity.this, MyNoticeBoardActivity.class);
					intentMyNoticeBoard.putExtra(Utility.DASHBOARD_ICON_ID,
							Utility.MY_NOTICE_BOARD);
					startActivity(intentMyNoticeBoard);
					DashboardScreenActivity.this.finish();
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.sendAFriendRLID:
			Intent intentSendaFriend = new Intent(DashboardScreenActivity.this,
					SendAFriendNewActivity.class);
			intentSendaFriend.putExtra(Utility.DASHBOARD_ICON_ID,
					Utility.SEND_A_FRIEND);
			startActivity(intentSendaFriend);
			break;
		case R.id.mySpecialOffersRLID:
			try {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentGIN1 = new Intent(DashboardScreenActivity.this,
							GrabitNowTabsActivity.class);
					intentGIN1.putExtra(Utility.DASHBOARD_ICON_ID, 1);
					startActivity(intentGIN1);
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.searchMyRewardsRLID:
			try {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentGIN2 = new Intent(DashboardScreenActivity.this,
							GrabitNowTabsActivity.class);
					intentGIN2.putExtra(Utility.DASHBOARD_ICON_ID, 2);
					startActivity(intentGIN2);
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.myParkingTimeRLID:
			try {
				Intent intentMyParking = new Intent(DashboardScreenActivity.this,
						NewParkingTimeActivity.class);
				intentMyParking.putExtra(Utility.DASHBOARD_ICON_ID,
						Utility.MY_PARKING_TIME);
				startActivity(intentMyParking);
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		case R.id.myissueRLID:
			Intent intentMyIssue = new Intent(DashboardScreenActivity.this,
					MyIssueActivity.class);
			intentMyIssue.putExtra(Utility.DASHBOARD_ICON_ID, Utility.MY_ISSUE);
			startActivity(intentMyIssue);
			break;
		case R.id.myAccountRLID:
			try {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					Intent intentMyAccount = new Intent(DashboardScreenActivity.this,
							MyAccountActivity.class);
					intentMyAccount.putExtra(Utility.DASHBOARD_ICON_ID,
							Utility.MY_ACCOUNT);
					startActivity(intentMyAccount);
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
								 
					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
				}
			} catch (Exception e) {
				if (e != null) {
					Log.w("Hari-->DEBUG", e);
					e.printStackTrace();
				}
			}
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// showDialog(LOGOUT);
			if (doubleBackToExitPressedOnce) {
				DashboardScreenActivity.this.finish();
				return doubleBackToExitPressedOnce;
			}
			doubleBackToExitPressedOnce = true;
			Toast.makeText(getApplicationContext(), "Press again to minimize the app.", 300).show();
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					doubleBackToExitPressedOnce = false;
				}
			}, 2500);
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}