package com.myrewards.au.awu.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.myrewards.au.awu.utils.Utility;

public class MyIssueActivity extends Activity implements OnClickListener,
		OnLongClickListener {
	EditText nameET, membershipET, messageET;
	Button sendbtn, deletebtn, backButton;
	TextView titleTV, myIssueNameTV, myIssueMemTV;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_issue);

		RelativeLayout headerImage = (RelativeLayout) findViewById(R.id.headerRLID);
		headerImage.getLayoutParams().height = (int) (Utility.screenHeight / 12.5);

		titleTV = (TextView) findViewById(R.id.titleTVID);
		titleTV.setTypeface(Utility.font_bold);

		myIssueNameTV = (TextView) findViewById(R.id.myIssueNameTVID);
		myIssueNameTV.setTypeface(Utility.font_bold);

		myIssueMemTV = (TextView) findViewById(R.id.myIssueMemNameTVID);
		myIssueMemTV.setTypeface(Utility.font_bold);

		nameET = (EditText) findViewById(R.id.nameETID);
		nameET.setTypeface(Utility.font_reg);
		backButton = (Button) findViewById(R.id.backBtnID);
		backButton.getLayoutParams().width = (int) (Utility.screenWidth / 8.5);
		backButton.getLayoutParams().height = (int) (Utility.screenHeight / 20.0);
		backButton.setOnClickListener(this);
		membershipET = (EditText) findViewById(R.id.membershipETID);
		membershipET.setTypeface(Utility.font_reg);
		messageET = (EditText) findViewById(R.id.messageETID);
		messageET.setTypeface(Utility.font_reg);
		sendbtn = (Button) findViewById(R.id.sendbtnID);
		sendbtn.getLayoutParams().width = (int) (Utility.screenWidth / 5.6);
		sendbtn.getLayoutParams().height = (int) (Utility.screenHeight / 18.0);
		sendbtn.setOnClickListener(this);
		deletebtn = (Button) findViewById(R.id.deletebtnID);
		sendbtn.getLayoutParams().width = (int) (Utility.screenWidth / 5.6);
		sendbtn.getLayoutParams().height = (int) (Utility.screenHeight / 18.0);
		deletebtn.setOnClickListener(this);
		deletebtn.getLayoutParams().width = (int) (Utility.screenWidth / 8);
		deletebtn.getLayoutParams().height = (int) (Utility.screenHeight / 13.3);
		nameET.setOnLongClickListener(this);
		membershipET.setOnLongClickListener(this);
		messageET.setOnLongClickListener(this);
		setEditBoxesText();
	}

	private void setEditBoxesText() {
		nameET.setText(Utility.user.getFirst_name() + " "
				+ Utility.user.getLast_name());
		membershipET.setText(Utility.user.getUsername());
	}

	@Override
	public void onClick(View arg0) {
		try {
			if (arg0.getId() == R.id.sendbtnID) {
				if (Utility.isOnline((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
					String toClientemail = getResources().getString(
							R.string.send_email_hint);
					//String toSreenEmail = getResources().getString(R.string.sreen_email_hint);
					String message = messageET.getText().toString();

					Intent email = new Intent(Intent.ACTION_SEND);
					email.putExtra(Intent.EXTRA_EMAIL, new String[] { toClientemail});
					email.putExtra(Intent.EXTRA_SUBJECT, "My Issue");
					email.putExtra(Intent.EXTRA_TEXT, message);

					// need this to prompts email client only
					email.setType("message/rfc822");

					startActivity(Intent.createChooser(email,
							"Choose an Email client :"));
				} else {
					// The Custom Toast Layout Imported here
					LayoutInflater inflater = getLayoutInflater();
					View layout = inflater.inflate(R.layout.toast_no_netowrk,
					(ViewGroup) findViewById(R.id.custom_toast_layout_id));
					//layout.getBackground().setAlpha(128);  // 50% transparent
					 
					// The actual toast generated here.
					Toast toast = new Toast(getApplicationContext());
					toast.setDuration(Toast.LENGTH_LONG);
					toast.setView(layout);
					toast.show();
					//showDialog(NO_NETWORK_CON);
				}
			}

			if (arg0.getId() == R.id.deletebtnID) {
				messageET.setText("");
			}
			if (arg0.getId() == R.id.backBtnID) {
				finish();
			}
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean onLongClick(View v) {
		boolean returnValue = true ;
		try {
			EditText ed = (EditText) v;
			int stringLength = ed.getText().length();
			returnValue = Utility.copyPasteMethod(v, stringLength);
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
		return returnValue;
	}
}
