package com.myrewards.au.awu.controller;

import org.jsoup.Jsoup;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.myrewards.au.awu.utils.Utility;

@SuppressLint("ShowToast")
public class SplashScreenActivity extends Activity {

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 3000;
	
	boolean newVersion = false;
	
	int currentAPILevel;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		currentAPILevel = Integer.valueOf(android.os.Build.VERSION.SDK);
		
		try
		{
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
			StrictMode.setThreadPolicy(policy); 
		}
		catch(Exception e)
		{
			if(e!=null)
			{
				e.printStackTrace();
			}
		}
		
		setTextFontSpace();

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */
			@Override
			public void run() {
				try {
					// Generating and Starting new intent on splash time out
					if (Utility.isOnline((ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE))) {
						if (!(currentAPILevel > 8)) {
							Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
							startActivity(intent);
							SplashScreenActivity.this.finish();
						}
						else {
							try {
								newVersion=web_update();
							} catch (Exception e) {
								Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
								startActivity(intent);
								SplashScreenActivity.this.finish();
							} catch (NoSuchMethodError e) {
								Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
								startActivity(intent);
								SplashScreenActivity.this.finish();
							}
							
							if (newVersion == true) {
								showDialog(1);
							} else {
								Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
								startActivity(intent);
								SplashScreenActivity.this.finish();
							}
						}
					} else {
						showDialog(2);
					}
					
				} catch (Exception e) {
					e.getStackTrace();
				}
			}

			private boolean web_update() {
				try {
					String package_name = getPackageName();
					String curVersion = getApplicationContext().getPackageManager()
							.getPackageInfo(package_name, 0).versionName;
					String newVersion = curVersion;
					newVersion = Jsoup
							.connect("https://play.google.com/store/apps/details?id="
											+ package_name + "&hl=en")
							.timeout(10000)
							.userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
							.referrer("http://www.google.com").get()
							.select("div[itemprop=softwareVersion]").first()
							.ownText();
					return (value(curVersion) < value(newVersion)) ? true : false;
				} catch (Exception e) {
					e.printStackTrace();
					return false;
				}
			}

			private Long value(String curVersion) {
				Long cV = null;
				try {
					curVersion = curVersion.trim();
					if (curVersion.contains(".")) {
						final int index = curVersion.lastIndexOf(".");
						cV = value(curVersion.substring(0, index)) * 100
								+ value(curVersion.substring(index + 1));
					} else {
						cV = Long.valueOf(curVersion);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				return cV;
			}
		}, SPLASH_TIME_OUT);
	}
	
	private void setTextFontSpace() {
		try {
			DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
			Utility.screenHeight = displaymetrics.heightPixels;
			Utility.screenWidth = displaymetrics.widthPixels;
			
			Utility.font_bold = Typeface.createFromAsset(this.getAssets(), "helvetica_bold.ttf");
			Utility.font_reg = Typeface.createFromAsset(this.getAssets(), "helvetica_reg.ttf");
		} catch (Exception e) {
		}
	}
	
	@Override
	protected Dialog onCreateDialog(int id) {
		if (id == 1) {
			AlertDialog callMobiledialog = null;
			switch (id) {
			case 1:
				LayoutInflater liYes = LayoutInflater.from(this);
				View callAddressView = liYes.inflate(
						R.layout.dialog_layout_update_version, null);
				AlertDialog.Builder adbrok = new AlertDialog.Builder(this);
				adbrok.setCancelable(false);
				adbrok.setView(callAddressView);
				callMobiledialog = adbrok.create();
				break;
			}
			return callMobiledialog;
		}
		else if (id == 2) {
			AlertDialog noNetworkDialog = null;
			LayoutInflater noNetInflater = LayoutInflater.from(this);
			View noNetworkView = noNetInflater.inflate(R.layout.dialog_layout_no_network, null);
			AlertDialog.Builder adbNoNet = new AlertDialog.Builder(this);
			adbNoNet.setCancelable(false);
			adbNoNet.setView(noNetworkView);
			noNetworkDialog = adbNoNet.create();
			return noNetworkDialog;
		}
		return super.onCreateDialog(id);
	}

	@Override
	protected void onPrepareDialog(int id, Dialog dialog) {

		switch (id) {
		case 1:
			final AlertDialog alt3 = (AlertDialog) dialog;

			TextView alertTitle = (TextView) alt3
					.findViewById(R.id.favGINTitleTVID);
			alertTitle.setTypeface(Utility.font_bold);

			TextView tv22 = (TextView) alt3.findViewById(R.id.addFavTVID);
			tv22.setTypeface(Utility.font_reg);
			tv22.setText(getResources().getString(R.string.update_version_message));
			Button addFavYesBtn = (Button) alt3
					.findViewById(R.id.add_fav_yesBtnID);
			addFavYesBtn.setTypeface(Utility.font_bold);
			Button addNoFavBtn = (Button) alt3
					.findViewById(R.id.add_fav_noBtnID);
			addNoFavBtn.setTypeface(Utility.font_bold);
			alt3.setCancelable(false);
			addFavYesBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					startActivity(new Intent(SplashScreenActivity.this,	AppPushNotificationActivity.class));
					SplashScreenActivity.this.finish();
					alt3.dismiss();
				}
			});
			addNoFavBtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					alt3.dismiss();
					Intent intent = new Intent(SplashScreenActivity.this, LoginScreenActivity.class);
					startActivity(intent);
					SplashScreenActivity.this.finish();
				}
			});
			break;
		case 2:
			final AlertDialog alertDialog2 = (AlertDialog) dialog;
			TextView textTv = (TextView) alertDialog2.findViewById(R.id.noConnTVID);
			
			textTv.setTypeface(Utility.font_reg);
			
			TextView alertTitle2 = (TextView) alertDialog2
					.findViewById(R.id.alertLogoutTitleTVID);
			
			alertTitle2.setTypeface(Utility.font_bold);
			
			Button okbutton = (Button) alertDialog2
					.findViewById(R.id.noNetWorkOKID);
			okbutton.setTypeface(Utility.font_bold);
			alertDialog2.setCancelable(false);
			okbutton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					SplashScreenActivity.this.finish();
					alertDialog2.dismiss();
				}
			});
		}
		super.onPrepareDialog(id, dialog);
	}

}
