package com.myrewards.au.awu.controller;

import java.util.List;
import java.util.Vector;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.myrewards.au.awu.model.HelpPageFive;
import com.myrewards.au.awu.model.HelpPageFour;
import com.myrewards.au.awu.model.HelpPageOne;
import com.myrewards.au.awu.model.HelpPageSix;
import com.myrewards.au.awu.model.HelpPageThree;
import com.myrewards.au.awu.model.HelpPageTwo;


/**
 * The <code>ViewPagerFragmentActivity</code> class is the fragment activity hosting the ViewPager  
 * @author mwho
 */
public class HelpPagesActivity extends FragmentActivity{
	/** maintains the pager adapter*/
	private PagerAdapter mPagerAdapter;
	/* (non-Javadoc)
	 * @see android.support.v4.app.FragmentActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.helpviewpager_layout);
		//initialsie the pager
		try {
			this.initialisePaging();
		} catch (Exception e) {
			if (e != null) {
				Log.w("Hari-->DEBUG", e);
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Initialise the fragments to be paged
	 */
	private void initialisePaging() {
		
		List<Fragment> fragments = new Vector<Fragment>();
		fragments.add(Fragment.instantiate(this, HelpPageOne.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageTwo.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageThree.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFour.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageFive.class.getName()));
		fragments.add(Fragment.instantiate(this, HelpPageSix.class.getName()));
		this.mPagerAdapter  = new PagerAdapter(super.getSupportFragmentManager(), fragments);
		//
		ViewPager pager = (ViewPager)super.findViewById(R.id.viewpager);
		pager.setAdapter(this.mPagerAdapter);
	}

}
